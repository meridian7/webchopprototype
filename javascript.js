function getURL() {
   chrome.tabs.query({ 'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT },
      function (tabs) {
         let fullURL = tabs[0].url;
         let domain = '';
         let webPage = '';
         let slashCounter = 0;
         for (i = 0; i < fullURL.length; i++) {
            if (fullURL[i] == '/' && slashCounter < 3) {
               slashCounter++;
            }
            if (slashCounter < 3 && slashCounter == 2) {
               domain += fullURL[i];
            } else if (slashCounter == 3) {
               webPage += fullURL[i];
            }
         }
         document.getElementById("domain").innerHTML = "Domain: " + domain;
         document.getElementById("page").innerHTML = "Page: " + webPage;
      }
   );
}
getURL();